App Name: Business Listings

Description: A simple business listings of company contacts using Google Firebase and Angular2

Installation:

- Install Angular CLI https://github.com/angular/angular-cli

- Install firebase https://github.com/angular/angularfire2

- Create a google firebase account and integrate the account information into src/main.ts 
 
